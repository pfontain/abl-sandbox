/**
 * Copyright 2013 UCSC
 */
package runner;

import simple.Agent;

/**
 *
 */
public class Runner {

   /**
    * @param args
    */
   public static void main(String[] args) {
      // new SimpleAgent().startBehaving(0,false);
      new Agent().startBehaving();
   }
}