/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wmes;

import wm.WME;

/**
 *
 * @author John
 */
public class AnotherWME extends WME{
    String field;
    String additional;
    
    public boolean assess(TestWME a){
        if(a.getField().equals(field) && !a.marked){
            return true;
        }
        System.out.println("False");
        return false;
    }
    
    
    public AnotherWME(String f, String b){
        field = f;
        additional = b;
    }
    
    public void setField(String f){
        field = f;
    }
    
    public String getField(){
        return field;
    }
    
    public void setAdditional(String b){
        additional = b;
    }
    
    public String getAdditional(){
        return additional;
    }
}
