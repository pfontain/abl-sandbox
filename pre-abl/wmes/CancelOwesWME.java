package wmes;

import wm.WME;

/**
 *
 * @author Paulo F Gomes
 */
public class CancelOwesWME extends WME{
    String characterInDebt;
    String characterInDebtTo;
    String objectOwed;

    public CancelOwesWME(String characterIndebt, String chracterInDebtTo, String objectOwed) {
        this.characterInDebt = characterIndebt;
        this.characterInDebtTo = chracterInDebtTo;
        this.objectOwed = objectOwed;
    }
    
    public String getCharacterInDebt(){
        return characterInDebt;
    }
    
    public String getCharacterInDebtTo(){
        return characterInDebtTo;
    }
    
    public String getObjectOwed(){
        return objectOwed;
    }
}
