package wmes;

/**
 *
 * @author Paulo Gomes
 */
public enum PersonalityType {
    LOW_CONCERN_SELF,
    HIGH_CONCERN_SELF
}
