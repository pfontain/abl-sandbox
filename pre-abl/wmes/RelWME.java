/*
 * 
 */
package wmes;

import wm.WME;

/**
 *
 * @author John
 */
public class RelWME extends WME{
    boolean used = false;
    
    String type; //The type of relationship
    String first; //The first person in relationship
    String second; // the second person in relationship, if any
    int value = -1; //The value, if any, of relationship.
    
    public RelWME(String t, String f, String s){
        type = t;
        first = f;
        second = s;
    }
    
    public RelWME(String t, String f, String s, int a){
        type = t;
        first = f;
        second = s;
        value = a;
    }
    
    public void setType(String t){
        type = t;
    }
    
    public void setFirst(String f){
        first = f;
    }
    
    public void setSecond(String s){
        second = s;
    }
    
    public String getType(){
        return type;
    }
    
    public String getFirst(){
        return first;
    }
    
    public String getSecond(){
        return second;
    }
    
    
    public void setUsed(Boolean b){
        used = b;
    }
    
    public boolean getUsed(){
        return used;
    }
    
}
