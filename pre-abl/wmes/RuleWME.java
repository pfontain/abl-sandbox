/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wmes;

import wm.WME;

/**
 *
 * @author John
 */
public class RuleWME extends WME{
    String ruleType;
    String firstRel;
    String secondRel;

    public RuleWME(String r, String f, String s) {
        ruleType = r;
        firstRel = f;
        secondRel = s;
    }
    
    public String getRuleType(){
        return ruleType;
    }
    
    public String getFirstRel(){
        return firstRel;
    }
    
    public String secondRel(){
        return secondRel;
    }
    
    public boolean assess(RelWME a, RelWME b){
     
        return true;
    }
    
}
