/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wmes;

import wm.WME;

/**
 *
 * @author John
 */
public class TestWME extends WME{
    String field;
    String field2;
    public boolean marked;
    
    public TestWME(String f){
        field = f;
    }
    
    public TestWME(String f, String b){
        field = f;
        field2 = b;
    }
    
    public void setField(String f){
        field = f;
    }
    
    public String getField(){
        return field;
    }
    
    public void setField2(String f){
        field2 = f;
    }
    
    public String getField2(){
        return field2;
    }
    
}
