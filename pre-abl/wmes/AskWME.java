/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wmes;

import wm.WME;

/**
 *
 * @author Paulo F Gomes
 */
public class AskWME extends WME{
    String asker;
    String asked;
    String objectRequested;

    public AskWME(String asker, String asked, String objectRequested) {
        this.asker = asker;
        this.asked = asked;
        this.objectRequested = objectRequested;
    }
    
    public String getAsker(){
        return asker;
    }
    
    public String getAsked(){
        return asked;
    }
    
    public String getObjectRequested(){
        return objectRequested;
    }
    
     public void setType(String asker){
        this.asker = asker;
    }
    
    public void setFirst(String asked){
        this.asked = asked;
    }
    
    public void setSecond(String objectRequested){
        this.objectRequested = objectRequested;
    }
}
