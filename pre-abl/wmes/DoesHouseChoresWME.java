package wmes;

import wm.WME;

public class DoesHouseChoresWME extends WME {
   private String character;


   public DoesHouseChoresWME(String character) {
      this.character = character;
   }

   public DoesHouseChoresWME() {
   }

   public synchronized String getCharacter() {
      return character;
   }

   public synchronized void setCharacter(String character) {
      this.character = character;
   }

}
