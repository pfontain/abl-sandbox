package wmes;

import wm.WME;

public class PersonalityWME extends WME {
   private String character;
   private PersonalityType type;


   public PersonalityWME(String character, PersonalityType type) {
      this.character = character;this.type = type;
   }

   public PersonalityWME() {
   }

   public synchronized String getCharacter() {
      return character;
   }

   public synchronized PersonalityType getType() {
      return type;
   }

   public synchronized void setCharacter(String character) {
      this.character = character;
   }

   public synchronized void setType(PersonalityType type) {
      this.type = type;
   }

}
