package wmes;

import wm.WME;

public class LivesWME extends WME {
   private String character;
   private String location;


   public LivesWME(String character, String location) {
      this.character = character;this.location = location;
   }

   public LivesWME() {
   }

   public synchronized String getCharacter() {
      return character;
   }

   public synchronized String getLocation() {
      return location;
   }

   public synchronized void setCharacter(String character) {
      this.character = character;
   }

   public synchronized void setLocation(String location) {
      this.location = location;
   }

}
