package wmes;

import wm.WME;

public class TiredChoresWME extends WME {
   private String character;


   public TiredChoresWME(String character) {
      this.character = character;
   }

   public TiredChoresWME() {
   }

   public synchronized String getCharacter() {
      return character;
   }

   public synchronized void setCharacter(String character) {
      this.character = character;
   }

}
