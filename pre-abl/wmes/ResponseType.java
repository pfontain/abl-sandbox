package wmes;

/**
 *
 * @author Paulo Gomes
 */
public enum ResponseType {
    INTEGRATING,
    ACCOMODATING,
    DOMINATING,
    AVOIDING
}
