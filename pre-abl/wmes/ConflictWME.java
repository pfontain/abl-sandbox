package wmes;

import wm.WME;

/**
 *
 * @author Paulo F Gomes
 */
public class ConflictWME extends WME{
    String characterA;
    String characterB;
    ConflictType type;
    
    
    // 
    public ConflictWME() {
    }

    public ConflictWME(ConflictType type, String characterA, String characterB) {
        this.characterA = characterA;
        this.characterB = characterB;
        this.type = type;
    }
    
    public String getCharacterA(){
        return characterA;
    }
    
    public String getCharacterB(){
        return characterB;
    }
    
    public ConflictType getType(){
        return type;
    }
}
