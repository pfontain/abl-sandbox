package wmes;

import wm.WME;

public class ConflictActionWME extends WME {
   private ConflictType conflictType;
   private ResponseType responseType;
   private String characterA;
   private String characterB;


   public ConflictActionWME(ConflictType conflictType, ResponseType responseType, String characterA, String characterB) {
      this.characterA = characterA;
      this.characterB = characterB;
      this.conflictType = conflictType;
      this.responseType = responseType;
   }

   public ConflictActionWME() {
   }

   public synchronized String getCharacterA() {
      return characterA;
   }

   public synchronized String getCharacterB() {
      return characterB;
   }

   public synchronized ConflictType getConflictType() {
      return conflictType;
   }

   public synchronized ResponseType getResponseType() {
      return responseType;
   }

   public synchronized void setCharacterA(String characterA) {
      this.characterA = characterA;
   }

   public synchronized void setCharacterB(String characterB) {
      this.characterB = characterB;
   }

   public synchronized void setConflictType(ConflictType conflictType) {
      this.conflictType = conflictType;
   }

   public synchronized void setResponseType(ResponseType responseType) {
      this.responseType = responseType;
   }

}
